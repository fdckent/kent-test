package com.test;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/testcall")
public class TestResource {

	@GET
	@Path("/test1")
	@Produces(MediaType.APPLICATION_JSON)
	public Response test() {
		return Response.ok("******************* We are now in the meeting!!!! ****************************").build();
	}	
}